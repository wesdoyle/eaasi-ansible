---
- name: check eaas components to update
  when: eaas_update_components != ''
  vars:
    eaas_components:
    - "{{ eaas_component_ui }}"
    - "{{ eaas_component_ear }}"
    - "{{ eaas_component_docker_image }}"
  assert:
    that: eaas_update_components is subset(eaas_components)
    success_msg: "Components to update: {{ eaas_update_components }}"
    fail_msg: "Invalid components to update: {{ eaas_update_components }}"

- name: create eaas home folder
  become: yes
  file:
    path: "{{ host_eaas_home }}"
    state: directory
    owner: "{{ ansible_user_uid }}"
    group: "{{ ansible_user_gid }}"

- name: create eaas runtime folders
  file:
    path: "{{ host_eaas_home }}/{{ item }}"
    state: directory
  loop:
  - config
  - certificates
  - deployments
  - eaas-ui
  - image-archive
  - server-data
  - log

- name: upload custom files
  when: eaas_deployment_mode != 'emucomp'
  loop: "{{ eaas_custom_files }}"
  include_tasks: custom-files.yaml
  vars:
    source_path: "{{ item.source }}"
    target_path: "{{ item.target }}"

- name: download pre-built eaas-server
  get_url:
    force: "{{ eaas_component_ear in eaas_update_components }}"
    url: "{{ eaas_server_ear_url }}"
    dest: "{{ host_eaas_home }}/deployments/eaas-server.ear"
    headers:
      PRIVATE-TOKEN: "{{ eaas_private_token  | default(omit) }}"

- name: check if db exists
  register: db_dir_check
  stat:
    path: "{{ host_eaas_home }}/server-data/db"

- name: upgrade image-archive's db
  when: db_dir_check.stat.exists and eaas_db_upgrade | bool
  include_tasks: db-upgrade.yaml
  vars:
    db_dir: "{{ host_eaas_home }}/server-data/db"
    db_backup_dir: "{{ db_dir }}-backup"
    db_version_file: "{{ db_dir }}-version.txt"

- name: prepare customized docker image
  import_tasks: docker/customize.yaml

- name: create directory for eaas-config parts
  file:
    path: "{{ host_eaas_config_parts_dir }}"
    state: directory

- name: prepare main eaas-config
  when: eaas_deployment_mode != 'emucomp'
  template:
    src: eaas-config.yaml.j2
    dest: "{{ host_eaas_config_parts_dir }}/01-main.yaml"

- name: generate random cluster-manager access-token
  when: eaas_cluster_api_access_token == ''
  set_fact:
    eaas_cluster_api_access_token: "{{ lookup('password', '/dev/null length=32 chars=ascii_letters,digits') }}"

- name: prepare cluster-manager config
  when: eaas_deployment_mode != 'emucomp'
  vars:
    clustermgr: "{{ eaas_clustermgr }}"
  template:
    src: "{{ eaas_clustermgr_config_template }}"
    dest: "{{ host_eaas_config_parts_dir }}/02-clustermgr.yaml"

- name: create directory for nginx configs
  file:
    path: "{{ host_eaas_config_nginx_dir }}"
    state: directory

- name: prepare main nginx-config
  when: eaas_deployment_mode != 'emucomp'
  template:
    src: nginx/nginx.conf
    dest: "{{ host_eaas_config_nginx_dir }}/nginx.conf"

- name: prepare main nginx-js
  when: eaas_deployment_mode != 'emucomp'
  template:
    src: nginx/proxy.js
    dest: "{{ host_eaas_config_nginx_dir }}/proxy.js"

- name: prepare admin-ui
  when: ui_enable_admin_ui and eaas_deployment_mode != 'emucomp'
  include_tasks: ui/admin-ui.yaml

- name: create systemd unit
  become: yes
  template:
    src: eaas.service.j2
    dest: "/etc/systemd/system/{{ host_eaas_service_name}}.service"

- name: reload systemd deamons
  become: yes
  systemd:
    daemon_reload: yes

- name: enable eaas.service
  become: yes
  service:
    name: "{{host_eaas_service_name}}.service"
    state: started
    enabled: yes
